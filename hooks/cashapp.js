import { useState, useEffect } from "react";
import { getAvatarUrl } from "../functions/getAvatarUrl";
import { WalletAdapterNetwork } from "@solana/wallet-adapter-base";
import { useConnection, useWallet } from "@solana/wallet-adapter-react";
import {
  LAMPORTS_PER_SOL,
  PublicKey,
  clusterApiUrl,
  Connection,
  SystemProgram,
  Transaction,
  Keypair,
} from "@solana/web3.js";

import BigNumber from "bignumber.js";
import NewTransactionModal from "../components/transaction/NewTransactionModal";

export const useCashApp = () => {
  const [userAddress, setUserAddress] = useState(
    "11111111111111111111111111111111"
  );
  const [avatar, setAvatar] = useState("");

  const [amount, setAmount] = useState(0);
  const [receiver, setReceiver] = useState("");
  const [transactionPurpose, setTransactionPurpose] = useState("");
  const [newTransactionModalOpen, setNewTransactionModalOpen] = useState(false);

  const { connected, publicKey, sendTransaction } = useWallet();
  const { connection } = useConnection();

  const useLocalStorage = (storageKey, fallbackState) => {
    const [value, setValue] = useState(
      JSON.parse(localStorage.getItem(storageKey)) ?? fallbackState
    );
    useEffect(() => {
      localStorage.setItem(storageKey, JSON.stringify(value));
    }, [value]);

    return [value, setValue];
  };

  const [transactions, setTransactions] = useLocalStorage("transactions", []);
  useEffect(() => {
    if (connected) {
      setAvatar(getAvatarUrl(publicKey.toString()));
      setUserAddress(publicKey.toString());
    }
  }, [connected]);

  // Create the transaction to send to our wallet and we can sign it from there!
  async function makeTransaction(fromWallet, toWallet, amount, reference) {
    console.log(fromWallet.toString(), " From Wallet");
    console.log(toWallet.toString(), " To Wallet");
    console.log(amount, " Amount");
    console.log(reference.toString(), " Reference");
    const network = WalletAdapterNetwork.Devnet;
    const endpoint = clusterApiUrl(network);
    console.log(endpoint);
    const connection = new Connection(endpoint);

    // Get a recent blockhas to include in the transaction
    const { blockhash } = await connection.getLatestBlockhash("finalized");
    console.log(blockhash);

    const transaction = new Transaction({
      recentBlockhash: blockhash,
      feePayer: fromWallet,
    });
    console.log(transaction);
    // Create teh instruction to send SOL from owner to recipient
    const transfetInstruction = SystemProgram.transfer({
      fromPubkey: fromWallet,
      lamports: amount.multipliedBy(LAMPORTS_PER_SOL).toNumber(),
      toPubkey: toWallet,
    });

    console.log(transfetInstruction);

    transfetInstruction.keys.push({
      pubkey: reference,
      isSigner: false,
      isWritable: false,
    });

    transaction.add(transfetInstruction);

    return transaction;
  }

  // Create the funciton to RUN the transaction. This will added to the button
  const doTransaction = async ({ amount, receiver, transactionPurpose }) => {
    const fromWallet = publicKey;
    const toWallet = new PublicKey(receiver);
    const bnAmount = new BigNumber(amount);
    const reference = Keypair.generate().publicKey;
    console.log(toWallet.toString());

    const transaction = await makeTransaction(
      fromWallet,
      toWallet,
      bnAmount,
      reference
    );
    console.log(transaction);
    const txnHash = await sendTransaction(transaction, connection);
    console.log(txnHash);

    const newId = (transactions.length + 1).toString();
    const newTransaction = {
      id: newId,
      form: {
        name: publicKey,
        handle: publicKey,
        avatar: avatar,
        varified: true,
      },
      to: {
        name: receiver,
        handle: "-",
        avatar: getAvatarUrl(receiver.toString()),
        varified: false,
      },
      description: transactionPurpose,
      transactionDate: new Date(),
      status: "Completed",
      amount: amount,
      source: "-",
      identifier: "-",
    };
    setNewTransactionModalOpen(false);
    setTransactions([newTransaction, ...transactions]);
  };

  return {
    connected,
    publicKey,
    avatar,
    userAddress,
    doTransaction,
    amount,
    setAmount,
    receiver,
    setReceiver,
    transactionPurpose,
    setTransactionPurpose,
    transactions,
    setTransactions,
    newTransactionModalOpen,
    setNewTransactionModalOpen,
  };
};

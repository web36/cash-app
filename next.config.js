/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  env: {
    RPC_NODE: "https://solana-devnet.g.alchemy.com/v2/" + process.env.API_KEY,
  },
};

module.exports = nextConfig;
